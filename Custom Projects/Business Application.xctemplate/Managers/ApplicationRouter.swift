//
//  ApplicationRouter.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import SwinjectStoryboard

final class ApplicationRouter: ApplicationRouterProtocol {
    
    private let storyboards: [Storyboards: UIStoryboard]
    private let window: UIWindow
    
    init(window: UIWindow,
        storyboards: [Storyboards: UIStoryboard]) {
        self.window = window
        self.storyboards = storyboards
    }
    
    func start() {
        // Do something else if needed at app start
        route()
    }
    
    func route() {
        #if DEBUG
            // Show the currently testing VC here (fast access)
            //            changeRootViewController(ofType: .checkoutPayment)
            //            return
        #endif
        
        // if userManager.isLoggedIn() {
             changeRootViewController(ofType: .main)
        // } else {
        //    changeRootViewController(ofType: .login)
        // }
    }
    
    func changeRootViewController(ofType type: ViewControllers) {
        change(rootViewController: viewController(ofType: type))
    }
    
    private func change(rootViewController: UIViewController) {
        window.rootViewController = rootViewController
    }
    
    func viewController(ofType type: ViewControllers) -> UIViewController {
        let storyboard = self.storyboards[type.storyboard]
        assert(storyboard != nil, "Storyboard should be registered before first use.")
        
        if let identifier = type.identifier {
            // If id is available, load the vc
            return storyboard!.instantiateViewController(withIdentifier: identifier)
        } else {
            // If not, load the initial vc of the storyboard
            return storyboard!.instantiateInitialViewController()!
        }
    }
    
}

fileprivate extension ViewControllers {
    
    var storyboard: Storyboards {
        switch self {
        case .main: return .main
        default: return .main
        }
    }
    
    var identifier: String? {
        switch self {
        case .main: return "MainViewController"
        default:
            return nil
        }
    }
}

