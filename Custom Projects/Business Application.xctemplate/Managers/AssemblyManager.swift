//
//  AssemblyManagerProtocol.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import Swinject
import SwinjectStoryboard

protocol AssemblyManagerProtocol {
    static var container: Container { get }
    func resetScope(scope: ObjectScope, in container: Container)
    func resetUserScopeInDefaultContainer()
}

final class AssemblyManager : AssemblyManagerProtocol {
    
    static let container: Container = SwinjectStoryboard.defaultContainer
    
    func resetScope(scope: ObjectScope, in container: Container) {
        container.resetObjectScope(scope)
    }
    
    func resetUserScopeInDefaultContainer() {
        resetScope(scope: ObjectScope.userScope, in: AssemblyManager.container)
    }
}

extension ObjectScope {
    // User specific scope for managing container scopes
    // static let userScope = ObjectScope(storageFactory: PermanentStorage.init)
}

