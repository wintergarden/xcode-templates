//
//  ApplicationRouterProtocol.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import UIKit

protocol ApplicationRouterDependentProtocol: class {
    var applicationRouter: ApplicationRouterProtocol! { get set }
}

protocol ApplicationRouterProtocol {
    func start()
    func route()
    
    func changeRootViewController(ofType type: ViewControllers)
    func viewController(ofType type: ViewControllers) -> UIViewController
}

enum ViewControllers {
    
    // Screens
    case main
}

enum Storyboards: String {
    case main = "Main"
}

extension Storyboards {
    static func all() -> [Storyboards] {
        return [
            main
        ]
    }
    
    var name: String {
        return rawValue
    }
}

