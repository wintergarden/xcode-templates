//
//  UXFlowPointChecker
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

/**
 An UXFlowPoint represents points in the entire user experience flow.
 It might contain certain screen entry points in the future,
 for example in order to show tutorial overlay on those screens
 or perform any other actions.
 */
enum UXFlowPoint: String {
    /// App Start event
    case appStart
    case enableLocation
    
    var key: String {
        return self.rawValue
    }
}

/**
 The purpose of this UXFlowPointChecker helper is to count how many times a user
 reached some points in the entire user experience flow.
 */

class UXFlowPointChecker {
    private static let userDefaults = UserDefaults.standard
    // We don't want to count after N number of times
    private static let maxReachNumber = 5
    
    /**
     Reach Flow Point function allows to increment a number of times a given point has been reached.
     This is mutating function, each call will increment the number of reaches.
     The function has a limit of reaches that will be count, see `maxReachNumber` constant.
     
     - parameter flowPoint:          The point in the UX flow that has been reached.
     
     - returns: The number of reaches of this Flow Point (including the reach of this function call).
     */
    static func reachFlowPoint(flowPoint: UXFlowPoint) -> Int {
        var reachedCount = userDefaults.object(forKey: flowPoint.key) as? Int ?? 0
        if reachedCount < maxReachNumber {
            reachedCount += 1
            reachFlowPoint(flowPoint: flowPoint, numberOfTimes: reachedCount)
        }
        return reachedCount
    }
    
    static func numberOfFlowReaches(flowPoint: UXFlowPoint) -> Int {
        return userDefaults.object(forKey: flowPoint.key) as? Int ?? 0
    }
    
    static func isFlowPointReached(flowPoint: UXFlowPoint) -> Bool {
        return numberOfFlowReaches(flowPoint: flowPoint) > 0
    }
    
    static func resetReachPoint(flowPoint: UXFlowPoint) {
        reachFlowPoint(flowPoint: flowPoint, numberOfTimes: 0)
    }
    
    private static func reachFlowPoint(flowPoint: UXFlowPoint, numberOfTimes: Int) {
        userDefaults.setValue(numberOfTimes, forKey: flowPoint.key)
        userDefaults.synchronize()
    }
}
