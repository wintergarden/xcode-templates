//
//  MainViewController.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import UIKit

protocol MainViewModelProtocol { }

protocol MainDataProviderProtocol {
    func getData() -> MainViewModelProtocol
}

final class MainViewController: UIViewController /*, ApplicationRouterDependentProtocol*/ {
    
    // MARK: - Constants
    
    fileprivate struct Constants {
        // static let cellIdentifier: String = "CellId"
    }
    
    // MARK: - Properties
    
    /*private*/ var dataProvider: MainDataProviderProtocol!
    
    // var applicationRouter: ApplicationRouterProtocol!
    
    // MARK: - IBOutlets
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methods
    
}
