//
//  MainDataProvider.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import Foundation

final class MainDataProvider: MainDataProviderProtocol {
    
    func getData() -> MainViewModelProtocol {
        return MainViewModel()
    }
}
