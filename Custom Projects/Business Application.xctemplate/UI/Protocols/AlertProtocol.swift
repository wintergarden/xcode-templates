//
//  AlertProtocol.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//


import UIKit

protocol AlertProtocol: LoaderProtocol {
    
    // Alerts - Only available for UIViewControllers, check extension
    
    // Errors - Only available for UIViewControllers, check extension
}

// MARK: - UIViewController: AlertProtocol extension

extension AlertProtocol where Self : UIViewController {
    
    // MARK: Default Alerts implementation
    
    /**
     Displays a simple AlertView
     - Parameters:
     - title: Title of the alert. Default: 'Alert'
     - message: Message of the alert.
     - cancel: Close button title. Default: 'Cancel'
     - Returns: The displeyed UIAlertController.
     */
    func showAlert(title: String = "alert.title".localized,
                   message: String? = nil,
                   cancel: String = "alert.cancel".localized) {
        
        let cancelAction = UIAlertAction(title: cancel, style: .cancel, handler: nil)
        showAlert(title: title, message: message, cancelAction: cancelAction)
    }
    
    /**
     Displays a simple AlertView with custom Cancel and custom Other actions.
     - Parameters:
     - title: Title of the alert. Default: 'Alert'
     - message: Message of the alert.
     - cancelAction: Close action.
     - otherActions: Other custom actions that can be selected as an option.
     - Returns: The displeyed UIAlertController.
     */
    func showAlert(title: String = "alert.title".localized,
                   message: String? = nil, cancelAction: UIAlertAction,
                   otherActions: UIAlertAction...) {
        
        let alertController = alert(
            title: title,
            message: message,
            cancelAction: cancelAction,
            actions: otherActions
        )
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Default ActionSheet implementation
    
    /**
     Displays a simple AlertView with custom Cancel and custom Other actions.
     - Parameters:
     - title: Title of the alert. Default: 'Alert'
     - message: Message of the alert.
     - cancelAction: Close action.
     - otherActions: Other custom actions that can be selected as an option.
     - Returns: The displeyed UIAlertController.
     */
    @discardableResult
    func showActionsheet(title: String? = nil,
                         message: String?,
                         cancelAction: UIAlertAction,
                         otherActions: UIAlertAction...) -> UIAlertController {
        
        return showActionsheet(
            title: title,
            message: message,
            cancelAction: cancelAction,
            otherActions: otherActions
        )
    }
    
    /**
     Displays a simple AlertView with custom Cancel and custom Other actions.
     - Parameters:
     - title: Title of the alert. Default: 'Alert'
     - message: Message of the alert.
     - cancelAction: Close action.
     - otherActions: Other custom actions that can be selected as an option.
     - Returns: The displeyed UIAlertController.
     */
    @discardableResult
    func showActionsheet(title: String? = nil,
                         message: String?,
                         cancelAction: UIAlertAction,
                         otherActions: [UIAlertAction]) -> UIAlertController {
        
        let alertController = alert(
            title: title,
            message: message,
            cancelAction: cancelAction,
            preferredStyle: .actionSheet,
            actions: otherActions
        )
        present(alertController, animated: true, completion: nil)
        return alertController
    }
    
    // MARK: Default Error implementation
    
    /**
     Displays a simple AlertView with a default 'Error' title and 'OK' close button.
     - Parameters:
     - title: Title of the alert. Default: 'Error'
     - message: Message of the alert.
     - cancel: Close button title. Default: 'OK'
     - Returns: The displeyed UIAlertController.
     */
    func showError(title: String = "error.title".localized,
                   message: String,
                   cancel: String = "error.ok".localized) {
        showAlert(title: title, message: message, cancel: cancel)
    }
    
    /**
     Displays a simple AlertView with a default 'Error' title and 'OK' close button, and a custom closing handler.
     - Parameters:
     - title: Title of the alert. Default: 'Error'
     - message: Message of the alert.
     - cancel: Close button title. Default: 'OK'
     - handler: A custom handler callback at dismissing the alertview.
     - Returns: The displeyed UIAlertController.
     */
    func showError(title: String = "error.title".localized,
                   message: String,
                   cancel: String = "error.ok".localized,
                   handler: ((UIAlertAction) -> Void)?) {
        
        let cancelAction = UIAlertAction(title: cancel, style: .cancel, handler: handler)
        showAlert(title: title, message: message, cancelAction: cancelAction)
    }
    
    /**
     Displays a simple AlertView with custom options & actions and with a default 'Error' title.
     - Parameters:
     - title: Title of the alert. Default: 'Error'
     - message: Message of the alert.
     - cancelActon: Close action.
     - otherActions: Other custom actions that can be selected as an option.
     - Returns: The displeyed UIAlertController.
     */
    func showError(title: String = "error.title".localized,
                   message: String,
                   cancelAction: UIAlertAction,
                   otherActions: UIAlertAction...) {
        showError(
            title: title,
            message: message,
            cancelAction: cancelAction,
            otherActions: otherActions
        )
    }
    
    func showError(title: String = "error.title".localized, errorObject error: Swift.Error) {
        showError(title: title, message: error.localizedDescription)
    }
    
    /**
     Displays a simple AlertView with custom options & actions and with a default 'Error' title.
     - Parameters:
     - title: Title of the alert. Default: 'Error'
     - message: Message of the alert.
     - cancelActon: Close action.
     - otherActions: Other custom actions that can be selected as an option.
     - Returns: The displeyed UIAlertController.
     */
    func showError(title: String = "error.title".localized,
                   message: String,
                   cancelAction: UIAlertAction,
                   otherActions: [UIAlertAction]) {
        let alertController = alert(
            title: title,
            message: message,
            cancelAction: cancelAction,
            actions: otherActions
        )
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Helpers
    
    func createAction(title: String,
                      style: UIAlertActionStyle = .default,
                      handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: title, style: style, handler: handler)
    }
    
    func cancelAction(handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: "alert.cancel".localized, style: .cancel, handler: handler)
    }

    private func alert(title: String?,
                       message: String?,
                       cancelAction: UIAlertAction,
                       preferredStyle: UIAlertControllerStyle = .alert,
                       actions: [UIAlertAction]? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: preferredStyle
        )
        
        if let actions = actions {
            actions.forEach { alertController.addAction($0) }
        }
        
        alertController.addAction(cancelAction)
        return alertController
    }
}


