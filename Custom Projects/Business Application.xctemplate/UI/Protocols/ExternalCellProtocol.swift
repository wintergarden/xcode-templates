//
//  ExternalCellProtocol.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import UIKit

protocol ExternalCellProtocol: class {
    
    static var nibName: String { get }
    
    static var identifier: String { get }
    
}

extension ExternalCellProtocol {
    
    static var nibName: String {
        return String(describing: self)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

protocol ExternalReusableView: ExternalCellProtocol {
    
    static var kind: String { get }
}

extension UICollectionView {
    
    func registerCell(_ externalCell: ExternalCellProtocol.Type) {
        register(
            UINib(nibName: externalCell.nibName, bundle: nil),
            forCellWithReuseIdentifier: externalCell.identifier
        )
    }
    
    func registerView(_ reusableView: ExternalReusableView.Type) {
        register(
            UINib(nibName: reusableView.nibName, bundle: nil),
            forSupplementaryViewOfKind: reusableView.kind,
            withReuseIdentifier: reusableView.nibName
        )
    }
    
    func dequeueExternalCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T?
        where T: ExternalCellProtocol {
            return self.dequeueReusableCell(
                withReuseIdentifier: T.identifier,
                for: indexPath) as? T
    }
    
    
    func dequeueExternalView<T: UICollectionReusableView>(for indexPath: IndexPath) -> T?
        where T: ExternalReusableView {
            return self.dequeueReusableSupplementaryView(
                ofKind: T.kind,
                withReuseIdentifier: T.identifier,
                for: indexPath) as? T
    }
}

extension UITableView {
    
    func register(externalCell: ExternalCellProtocol.Type) {
        register(
            UINib(nibName: externalCell.nibName, bundle: nil),
            forCellReuseIdentifier: externalCell.identifier
        )
    }
}
