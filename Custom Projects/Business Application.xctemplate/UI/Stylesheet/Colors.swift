//
//  Colors.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import Foundation
import Hue

struct Colors {
    
    static let primary = UIColor(hex: <#ColorHexCode#>)
    static let secondary = UIColor(hex: <#ColorHexCode#>)
    
//    static let primaryText = UIColor(hex: <#ColorHexCode#>)
//    static let secondaryText = UIColor(hex: <#ColorHexCode#>)
//    
//    static let primaryBackground = UIColor(hex: <#ColorHexCode#>)
//    static let secondaryBackground = UIColor(hex: <#ColorHexCode#>)
//
//    static let navigationBar = UIColor(hex: <#ColorHexCode#>)
//    
//    static let error =  UIColor(hex: <#ColorHexCode#>)

    static let white = UIColor.white
    static let black = UIColor.black
    static let clear = UIColor.clear
}
