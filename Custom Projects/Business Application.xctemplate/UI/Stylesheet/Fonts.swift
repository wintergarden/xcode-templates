//
//  Fonts.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import UIKit

enum FontStyle {
    
    case title
    case subtitle
    case body
    case button
    
    var font: UIFont {
        switch self {
        case .title: return Fonts.mainFont(type: <#.regular#>, size: <#.title#>)
        case .subtitle: return Fonts.mainFont(type: <#.regular#>, size: <#.title#>)
        case .body: return Fonts.mainFont(type: <#.regular#>, size: <#.title#>)
        case .button: return Fonts.mainFont(type: <#.regular#>, size: <#.title#>)
        }
    }
}

// MARK: - Helpers

enum FontSize {
    
    case title
    case subtitle
    case body
    case button
    
    var value: CGFloat {
        switch self {
        case .title: return <#18#>
        case .subtitle: return <#15#>
        case .body: return <#12#>
        case .button: return <#14#>
        }
    }
}

enum FontType: String {
    
    case ultraLight
    case ultraLightItalic
    case thin
    case thinItalic
    case light
    case lightItalic
    case regular
    case regularItalic
    case medium
    case mediumItalic
    case semibold
    case semiboldItalic
    case bold
    case boldItalic
    case heavy
    case heavyItalic
    
}

struct Fonts {
    
    private struct Constants {
        static let mainFontName: String = <#MainFontFamilyName#>
        static let secondaryFontName: String = <#SecondaryFontFamilyName#>
    }
    
    static func mainFont(type: FontType = .regular, size: FontSize = .body) -> UIFont {
        let name = "\(Constants.mainFontName)-\(type.rawValue.capitalized)"
        return UIFont(name: name, size: size.value)!
    }
    
    static func secondaryFont(type: FontType = .regular, size: FontSize = .body) -> UIFont {
        let name = "\(Constants.secondaryFontName)-\(type.rawValue.capitalized)"
        return UIFont(name: name, size: size.value)!
    }
}
