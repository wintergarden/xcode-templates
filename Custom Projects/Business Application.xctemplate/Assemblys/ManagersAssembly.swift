//
//  ManagersAssembly.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Swinject

final class ManagersAssembly: Assembly {
    
    func assemble(container: Container) {
        
        // Application Router
        container.register(ApplicationRouterProtocol.self) { r in
            // Resolve storyboards:
            var storyboards: [Storyboards: UIStoryboard] = [:]
            for storyboard in Storyboards.all() {
                storyboards[storyboard] = r.resolve(UIStoryboard.self, name: storyboard.name)
            }
            
            return ApplicationRouter(
                window: r.resolve(UIWindow.self)!,
                storyboards: storyboards
            )
            }.inObjectScope(.container)
        
        // Assembly manager
        container.register(AssemblyManagerProtocol.self) { r in
            return AssemblyManager()
        }
    }
    
}

/*
 final class <#AssemblyType#>Assembly: AssemblyType {
 
 func assemble(container: Container) {
 
 /*
 // <#Name#>
 container.register(<#Protocol Type#>.self) { r in
 return <#Instance Type#>
 }
 */
 }
 
 }
 */
