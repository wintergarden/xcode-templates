//
//  DIManager.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

import Swinject
import SwinjectStoryboard

import Fabric
import Crashlytics

final class DIManager {
    
    static let shared = DIManager()
    
    fileprivate let assembler = Assembler(container: AssemblyManager.container)
    
    private init() {
        // Register assemblys
        assembler.apply(assemblies: [
            ApplicationAssembly(),
            ManagersAssembly(),
            ServicesAssembly(),
            ])
        
        initDependencies()
    }
    
    private func initDependencies() {
        //let resolver = assembler.resolver
    }
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        
        Fabric.with([Crashlytics.self])
        
        // Check first app start
        if UXFlowPointChecker.reachFlowPoint(flowPoint: .appStart) <= 1 {
            // Delete all stored properties
            SecurePropertyCleaner.clean()
        }
    }
    
}

extension DIManager {
    /**
     Easy Access to the Application Resolver.
     Should be used to instantiate registered objects.
     
     Exsample usage:
     
     `ManagerProvider.ApplicationResolver.resolve(SomeProtocol.self)`
     */
    static var ApplicationResolver: Resolver {
        return shared.assembler.resolver
    }
    
    /**
     Easy access & resolve registered dependencies.
     
     Use the service parameter for explicit type declaration or leave it blank
     to resolve by the context.
     
     - returns: The requested resource.
     */
    class func resolve<T>(service: T.Type? = nil) -> T {
        return ApplicationResolver.resolve(T.self)!
    }
    
    /**
     Easy access & resolve registered dependencies.
     
     Retrieves the instance with the specified service type and registration name.
     
     - service: The service type to resolve.
     - name: The registration name.
     
     - returns: The resolved service type instance, or nil if no service with the
     name is found.
     */
    class func resolve<T>(service: T.Type, name: String?) -> T? {
        return ApplicationResolver.resolve(T.self, name: name)
    }
    
    class func resolve<Service, Arg1>(
        _ serviceType: Service.Type,
        argument: Arg1) -> Service? {
        return ApplicationResolver.resolve(Service.self, argument: argument)
    }
}
