//
//  ApplicationAssembly.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Swinject
import SwinjectStoryboard

final class ApplicationAssembly: Assembly {
    
    func assemble(container: Container) {
        registerWindow(container: container)
        registerStoryboards(container: container)
        registerViewControllers(container: container)
    }
    
    private func registerWindow(container: Container) {
        container.register(UIWindow.self) { (r) -> UIWindow in
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.makeKeyAndVisible()
            return window
            }.inObjectScope(.container)
    }
    
    private func registerStoryboards(container: Container) {
        Storyboards.all().forEach {
            storyboard in
            
            container.register(
                UIStoryboard.self,
                name: storyboard.name,
                factory: { (r) -> UIStoryboard in
                    SwinjectStoryboard.create(
                        name: storyboard.name,
                        bundle: nil,
                        container: container)
            }).inObjectScope(.container)
        }
    }
    
    private func registerViewControllers(container: Container) {
        // Register all viewcontrollers here (that need DI)
        
        /*
         // <#Name #>
         container.registerForStoryboardProject(controllerType: <#View Controller#>.self) {
         r, c in
         <#Resolve extra dependencies#>
         }
         */
        
        // Main
        container.registerForStoryboardProject(controllerType: MainViewController.self){
            r, c in
            c.dataProvider = r.resolve(MainDataProviderProtocol.self)!
        }

    }
}

fileprivate extension Container {
    
    func registerForStoryboardProject<C:Controller>(controllerType: C.Type, name: String? = nil, initCompleted: ((Resolver, C) -> ())? = nil) {
        self.storyboardInitCompleted(controllerType, name: name) { (r, c) in
            
            // Resolve known dependencies
            
            if let c = c as? ApplicationRouterDependentProtocol {
                c.applicationRouter = r.resolve(ApplicationRouterProtocol.self)
            }
            
            // Call additional resolver
            initCompleted?(r, c)
        }
    }
    
}
