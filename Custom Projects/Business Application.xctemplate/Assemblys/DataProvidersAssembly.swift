//
//  DataProvidersAssembly.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Swinject

final class DataProvidersAssembly: Assembly {
    
    func assemble(container: Container) {

        container.register(MainDataProviderProtocol.self) { resolver in
            return MainDataProvider()
        }
        
    }
    
}
