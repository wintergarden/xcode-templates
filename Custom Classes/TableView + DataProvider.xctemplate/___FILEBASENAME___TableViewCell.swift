//
//  ___FILENAME___TableViewCell.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import UIKit

// MARK: - View model protocol

protocol ___VARIABLE_itemType___CellViewModelProtocol {
    
}

// MARK: - Cell

final class ___VARIABLE_itemType___TableViewCell: UITableViewCell {
    
    // MARK: - Constants
    
    // MARK: - Properties
    
    var ___VARIABLE_itemName___: ___VARIABLE_itemType___CellViewModelProtocol? {
        didSet { render(with: ___VARIABLE_itemName___ ) }
    }
    
    // MARK: - IBOutlets
    
    // MARK: - Lifecycle methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methdos
    
    private func render(with ___VARIABLE_itemName___: ___VARIABLE_itemType___CellViewModelProtocol?) {
        guard let ___VARIABLE_itemName___ = ___VARIABLE_itemName___ else { return clear() }
        
    }
    
    private func clear() {
        
    }
    
}
