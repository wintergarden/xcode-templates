//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//
import UIKit


// MARK: - DataProvider protocol

protocol ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol {
    
    var numberOfItems: Int { get }
    
    func ___VARIABLE_itemName___(at indexPath: IndexPath) -> ___VARIABLE_itemType___CellViewModelProtocol
}

// MARK: - ViewController

final class ___FILEBASENAMEASIDENTIFIER___ViewController: UIViewController {
    
    // MARK: - Constants
    
    fileprivate struct Constants {
        
        struct TableView {
            
            static let cellIdentifier = "___VARIABLE_itemType___CollectionViewCell"
            
            static let rowHeight: CGFloat = <#rowHeight#>
        }
    }
    
    // MARK: - Properties
    
    fileprivate var dataProvider: ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol!
    
    // MARK: - IBOutlets
    
    @IBOutlet weak fileprivate var tableView: UITableView!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methdos
    
}

// MARK: - UICollectionView DataSource & Delegate methods

extension ___FILEBASENAMEASIDENTIFIER___ViewController: UITableViewDataSource, UITableViewDelegate {
    
    // DataSource methods
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return dataProvider.numberOfItems
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.TableView.cellIdentifier, for: indexPath) as? ___VARIABLE_itemType___TableViewCell else {
                assertionFailure("Setup cell")
                return UITableViewCell()
        }
        
        cell.___VARIABLE_itemName___ = dataProvider.___VARIABLE_itemName___(at: indexPath)
        return cell
    }
    
    // Delegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.TableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
