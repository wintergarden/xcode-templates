//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

// MARK: - DataProvider protocol

protocol ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol {
    
    var numberOfItems: Int { get }
    
    func ___VARIABLE_itemName___(at indexPath: IndexPath) -> ___VARIABLE_itemType___CellViewModelProtocol
}

// MARK: - ViewController

final class ___FILEBASENAMEASIDENTIFIER___ViewController: UIViewController {
    
    // MARK: - Constants
    
    fileprivate struct Constants {
        
        struct CollectionView {
            
            static let cellIdentifier = "___VARIABLE_itemType___CollectionViewCell"
            
            static let cellSize = CGSize(width: <#CellWidth#>, height: <#Cellheight#>)
            
            static let lineSpacing: CGFloat = <#Spacing#>
        }
    }
    
    // MARK: - Properties
    
    fileprivate var dataProvider: ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol!
    
    // MARK: - IBOutlets
    
    @IBOutlet weak fileprivate var collectionView: UICollectionView!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methdos
    
}

// MARK: - UICollectionView DataSource & Delegate methods

extension ___FILEBASENAMEASIDENTIFIER___ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // DataSource methods
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return dataProvider.numberOfItems
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: Constants.CollectionView.cellIdentifier, for: indexPath) as? ___VARIABLE_itemType___CollectionViewCell else {
                assertionFailure("Setup cell")
                return UICollectionViewCell()
        }
        
        cell.___VARIABLE_itemName___ = dataProvider.___VARIABLE_itemName___(at: indexPath)
        return cell
    }
    
    
    // Layout delegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Constants.CollectionView.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionView.lineSpacing
    }
    
    // Delegate methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
