//
//  ___FILENAME___DataProvider.swift
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

final class ___FILEBASENAMEASIDENTIFIER___DataProvider: ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol {
    
    // MARK: - Properties
    
    private var ___VARIABLE_itemName___s = [___VARIABLE_itemType___CellViewModelProtocol]()
    
    var numberOfItems: Int {
        return ___VARIABLE_itemName___s.count
    }
    
    // MARK: - Lifecycle methods
    
    init() {}
    
    // MARK: - Protocol implementations
    
    func ___VARIABLE_itemName___(at indexPath: IndexPath) -> ___VARIABLE_itemType___CellViewModelProtocol {
        return ___VARIABLE_itemName___s[indexPath.item]
    }
    
    // MARK: - Helper methods
    
}
