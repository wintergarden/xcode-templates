//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

// MARK: - Service protocol

protocol ___FILEBASENAMEASIDENTIFIER___ServiceProtocol {
  
}

// MARK: - Service implementation

final class ___FILEBASENAMEASIDENTIFIER___Service: ___FILEBASENAMEASIDENTIFIER___ServiceProtocol {
    
}
