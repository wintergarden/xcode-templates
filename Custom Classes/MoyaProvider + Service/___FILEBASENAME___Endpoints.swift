//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Moya

enum ___FILEBASENAMEASIDENTIFIER___Endpoints {
  // case .someEncpoint(withParam: String)
}

extension ___FILEBASENAMEASIDENTIFIER___Endpoints: TargetType {
  
  var baseURL: URL {
    // TODO: replace url string
    return URL(string: "")!
  }
  
  var path: String {
    switch self {
    default: return ""
    }
  }
  
  var method: Moya.Method {
    switch self {
    default: return .get
    }
  }
  
  var task: Task {
    return .request
  }
  
  var validate: Bool {
    return true
  }
  
  var parameters: [String: Any]? {
    switch self {
    default: return nil
    }
  }
  
  var parameterEncoding: ParameterEncoding {
    return URLEncoding.default
  }
  
}

extension ___FILEBASENAMEASIDENTIFIER___Endpoints {
  
  // Sample data for testin
  var sampleData: Data {
    return Data()
  }
}
