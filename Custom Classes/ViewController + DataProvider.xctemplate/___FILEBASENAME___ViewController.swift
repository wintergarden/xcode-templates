//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

// MARK: - ViewModel Protocol

protocol ___FILEBASENAMEASIDENTIFIER___ViewModelProtocol {
    
}

// MARK: - Data Provider Protocol


protocol ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol {
    
}

// MARK: - View Controller implementation


final class ___FILEBASENAMEASIDENTIFIER___ViewController: UIViewController {
    
    // MARK: - Constants
    
    // MARK: - Properties
    
    var dataProvider: ___FILEBASENAMEASIDENTIFIER___DataProviderProtocol!
    
    // MARK: - IBOutlets
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - IBActions
    
    // MARK: - Private methdos
    
    
    private func render(with ___FILEBASENAMEASIDENTIFIER___: ___FILEBASENAMEASIDENTIFIER___ViewModelProtocol?) {
        guard let ___FILEBASENAMEASIDENTIFIER___ = ___FILEBASENAMEASIDENTIFIER___ else { return clear() }
        
    }
    
    private func clear() {
        
    }
}
