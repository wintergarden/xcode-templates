//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Alamofire

protocol ___VARIABLE_projectCodeName___ServerMethod: ServerMethod {
    var additionalPath: String? { get }
    var lastPath: String? { get }
}

extension ___VARIABLE_projectCodeName___ServerMethod {
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var httpMethod: HTTPMethod {
        return .post
    }
    
    var additionalPath: String? {
        return nil
    }
    
    var lastPath: String? {
        return nil
    }
    
    var path: String {
        var pathComponents: [String] = []
        if let additionalPath = additionalPath {
            pathComponents.append(additionalPath)
        }
        if let lastPath = lastPath {
            pathComponents.append(lastPath)
        }
        return pathComponents.joined(separator: "/")
    }
    
}

struct API {
    
    static let baseUrlString = "<#App Base Url#>"
    static var baseUrl: URL { return URL(string: baseUrlString)! }
    
    struct Method {
        
        /*
        enum <#Method Type#>: ___VARIABLE_projectCodeName___ServerMethod {
            case <#Method Name#>
        }
         */
        
    }
}
